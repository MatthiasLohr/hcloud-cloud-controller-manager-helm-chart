<mark>
<strong>IMPORTANT!</strong> This Helm Chart is not maintained anymore in favor of the official Helm Chart now provided by Hetzner, located at https://github.com/hetznercloud/csi-driver/tree/main/chart.
</mark>


# Helm Chart for Hetzner Cloud hcloud-cloud-controller-manager

This is a community Helm Chart for installing the hcloud-cloud-controller-manager in your Hetzner Cloud Kubernetes cluster.
The original sources of the hcloud-cloud-controller-manager can be found at
[https://github.com/hetznercloud/hcloud-cloud-controller-manager](https://github.com/hetznercloud/hcloud-cloud-controller-manager).

**Please note**: This project is a community project from a Hetzner customer, published for use by other Hetzner customers.
Neither the author nor this project is affiliated with Hetzner Online GmbH.


## Installation

### Add Helm Repository

```
helm repo add mlohr https://helm-charts.mlohr.com/
helm repo update
```

### Install to Kubernetes

After your Kubernetes cluster is installed, you can install hcloud-cloud-controller-manager using this quick example:

  * Without using a private network:
    ```
    helm install -n kube-system hcloud-cloud-controller-manager mlohr/hcloud-cloud-controller-manager \
      --set secret.hcloudApiToken=<HCLOUD API TOKEN>
    ```
  * With using a private network:
    ```
    helm install -n kube-system hcloud-cloud-controller-manager mlohr/hcloud-cloud-controller-manager \
      --set secret.hcloudApiToken=<HCLOUD API TOKEN> \
      --set config.privateNetworks.enabled=true \
      --set config.privateNetworks.id=<HCLOUD NETWORK ID> \
      --set config.privateNetworks.subnet=<HCLOUD NETWORK CIDR>
    ```
  * You can also (re)use an existing secret, e.g. from [hcloud-csi-driver Helm Chart](https://gitlab.com/MatthiasLohr/hcloud-csi-driver-helm-chart):
    ```
    helm install -n kube-system hcloud-cloud-controller-manager mlohr/hcloud-cloud-controller-manager \
      --set secret.existingSecretName=<EXISTING SECRET NAME>
    ```


## Configuration

To see all available configuration options for a deployment using this helm chart,
please check the [`values.yaml`](https://gitlab.com/MatthiasLohr/hcloud-cloud-controller-manager-helm-chart/-/blob/main/values.yaml) file.


### LoadBalancers

The `hcloud-cloud-controller-manager` allows for automatic instantiation and configuration of Hetzner load balancer instances for Kubernetes `Service` resource with type `LoadBalancer` (enabled by default).
The load balancer instance can be configured by adding annotations to the `Service` resource.
Check https://github.com/hetznercloud/hcloud-cloud-controller-manager/blob/master/internal/annotation/load_balancer.go for a list of supported annotations.


## License

This project is published under the Apache License, Version 2.0.
See [LICENSE.md](https://gitlab.com/MatthiasLohr/hcloud-cloud-controller-manager-helm-chart/-/blob/master/LICENSE.md) for more information.

Copyright (c) by [Matthias Lohr](https://mlohr.com/) &lt;[mail@mlohr.com](mailto:mail@mlohr.com)&gt;
